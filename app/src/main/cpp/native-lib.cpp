#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <stdio.h>
#include<jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <android/log.h>
#include <opencv2/text.hpp>

std::vector<cv::Rect> detectLetters(cv::Mat img)
{
    std::vector<cv::Rect> boundRect;
    cv::Mat img_gray, img_sobel, img_threshold, element;
    cvtColor(img, img_gray, CV_RGBA2GRAY);
    inRange(img_gray, (0, 0, 0), (179,50, 100), img_gray);
    cv::Sobel(img_gray, img_sobel, CV_8U, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
    cv::threshold(img_sobel, img_threshold, 0, 255, CV_THRESH_OTSU+CV_THRESH_BINARY);
    element = getStructuringElement(cv::MORPH_RECT, cv::Size(17, 3) );
    cv::morphologyEx(img_threshold, img_threshold, CV_MOP_CLOSE, element); //Does the trick
    std::vector< std::vector< cv::Point> > contours;
    cv::findContours(img_threshold, contours, 0, 1);
    std::vector<std::vector<cv::Point> > contours_poly( contours.size() );
    for( int i = 0; i < contours.size(); i++ )
        if (contours[i].size()>100)
        {
            cv::approxPolyDP( cv::Mat(contours[i]), contours_poly[i], 9, true );
            cv::Rect appRect( boundingRect( cv::Mat(contours_poly[i]) ));
            if (appRect.width>appRect.height)
                boundRect.push_back(appRect);
        }
    return boundRect;
}

using namespace std;
using namespace cv;
using namespace cv::text;

std::string ConvertJString(JNIEnv* env, jstring str)
{
    const jsize len = env->GetStringUTFLength(str);
    const char* strChars = env->GetStringUTFChars(str, (jboolean *)0);

    std::string Result(strChars, len);

    env->ReleaseStringUTFChars(str, strChars);

    return Result;
}

Rect biggestCountour(vector<vector<cv::Point> > contours){

    Rect bounding_rect;
    int largest_area=0;
    int largest_contour_index=0;
    double maxValCoun = 0;
    for( size_t i = 0; i< contours.size(); i++ ) // iterate through each contour.
    {
        double area = contourArea( contours[i] );  //  Find the area of contour

        if( area > largest_area )
        {
            largest_area = area;
            largest_contour_index = i;               //Store the index of largest contour
            bounding_rect = boundingRect( contours[i] ); // Find the bounding rectangle for biggest contour
        }
    }
    return bounding_rect;
}

extern "C"
{
void JNICALL Java_tritux_opencvnative_MainActivity_salt(JNIEnv *env, jobject instance,
                                                                           jlong matAddrGray,
                                                                           jint nbrElem) {
    Mat &mGr = *(Mat *) matAddrGray;
    for (int k = 0; k < nbrElem; k++) {
        int i = rand() % mGr.cols;
        int j = rand() % mGr.rows;
        mGr.at<uchar>(j, i) = 255;
    }
}

void JNICALL Java_tritux_opencvnative_MainActivity_detectObject(JNIEnv *env, jobject instance,
                                                        jlong img_object,
                                                        jlong img_scene){

    Mat descriptors_scene = *(Mat *) img_scene;
    Mat descriptors_object = *(Mat *) img_object;
    cvtColor(descriptors_object, descriptors_object, COLOR_RGB2GRAY);

    Ptr<ORB> detector = ORB::create();
    //Mat descriptors_object, descriptors_scene;
    std::vector<KeyPoint> keypoints_object, keypoints_scene;
    detector->detectAndCompute( descriptors_scene, Mat(), keypoints_object, descriptors_object );
    detector->detectAndCompute( descriptors_scene, Mat(), keypoints_scene, descriptors_scene );

    if(descriptors_object.type()!=CV_32F) {
        descriptors_object.convertTo(descriptors_object, CV_32F);
    }

    if(descriptors_scene.type()!=CV_32F) {
        descriptors_scene.convertTo(descriptors_scene, CV_32F);
    }
    //-- Step 2: Matching descriptor vectors using FLANN matcher
    FlannBasedMatcher matcher;
    std::vector< DMatch > matches;
    matcher.match( descriptors_object, descriptors_scene, matches );

    double max_dist = 0; double min_dist = 100;

//-- Quick calculation of max and min distances between keypoints
    for( int i = 0; i < descriptors_object.rows; i++ )
    { double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }

    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
    std::vector< DMatch > good_matches;
    for( int i = 0; i < descriptors_object.rows; i++ )
    { if( matches[i].distance < 3*min_dist )
        { good_matches.push_back( matches[i]); }
    }

 /*   Mat img_matches;
    drawMatches( img_object, keypoints_object, img_scene, keypoints_scene,
                 good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                 std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );*/

    //-- Localize the object
    std::vector<Point2f> obj =  Mat(4, 1, CV_32FC2);
    std::vector<Point2f> scene =  Mat(4, 1, CV_32FC2);
    for( size_t i = 0; i < good_matches.size(); i++ )
    {
        //-- Get the keypoints from the good matches
        obj.push_back( keypoints_object[ good_matches[i].queryIdx ].pt );
        scene.push_back( keypoints_scene[ good_matches[i].trainIdx ].pt );
    }

    Mat H = findHomography( obj, scene, CV_RANSAC);

    //-- Get the corners from the image_1 ( the object to be "detected" )
    std::vector<Point2f> obj_corners = Mat(4, 1, CV_32FC2);;
    obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( descriptors_object.cols, 0 );
    obj_corners[2] = cvPoint( descriptors_object.cols, descriptors_object.rows ); obj_corners[3] = cvPoint( 0, descriptors_object.rows );
    std::vector<Point2f> scene_corners = Mat(4, 1, CV_32FC2);;

    // Mat obj_corners =  Mat(4,1,CV_32FC2);
    //Mat scene_corners =  Mat(4,1,CV_32FC2);

    if(!H.empty()){

        perspectiveTransform( obj_corners, scene_corners, H);

         //-- Show detected matches
        //imshow( "Good Matches & Object detection", img_matches );
        // waitKey(0);
    }
    putText(descriptors_scene, "[ Detected CIN]",  scene_corners[0] + Point2f( descriptors_object.cols, 0) , FONT_HERSHEY_SIMPLEX, 1.0, Scalar(0, 255, 0));
    //-- Draw lines between the corners (the mapped object in the scene - image_2 )
    line( descriptors_scene, scene_corners[0] + Point2f( descriptors_object.cols, 0), scene_corners[1] + Point2f( descriptors_object.cols, 0), Scalar(255, 0, 0), 3 );
    line( descriptors_scene, scene_corners[1] + Point2f( descriptors_object.cols, 0), scene_corners[2] + Point2f( descriptors_object.cols, 0), Scalar( 255, 0, 0), 3 );
    line( descriptors_scene, scene_corners[2] + Point2f( descriptors_object.cols, 0), scene_corners[3] + Point2f( descriptors_object.cols, 0), Scalar( 255, 0, 0), 3 );
    line( descriptors_scene, scene_corners[3] + Point2f( descriptors_object.cols, 0), scene_corners[0] + Point2f( descriptors_object.cols, 0), Scalar( 255, 0, 0), 3 );

   // return  img_scene;
}

/**********************| MRZ |******************************/
JNIEXPORT void JNICALL Java_tritux_opencvnative_DetectActivity_detectMRZNative(JNIEnv *env, jobject instance,
                                                                        jlong addrRgba, jstring path) {

    Mat rgb = *(Mat *) addrRgba;
    Mat gray;
    Mat im_bw;
    cvtColor(rgb, gray, CV_BGR2GRAY);
    adaptiveThreshold(gray,im_bw,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,11,25);// for image output (OCR)
    //threshold(gray, im_bw, 128, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);// for image output (OCR)

    Mat grad;
    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
    morphologyEx(gray, grad, MORPH_GRADIENT, morphKernel);

    Mat bw;
    threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);

    // connect horizontally oriented regions
    Mat connected;
    morphKernel = getStructuringElement(MORPH_RECT, Size(9, 1));
    morphologyEx(bw, connected, MORPH_CLOSE, morphKernel);

    // find contours
    Mat mask = Mat::zeros(bw.size(), CV_8UC1);
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(connected, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

    vector<Rect> mrz;
    double r = 0;
    // filter contours
    for(int idx = 0; idx >= 0; idx = hierarchy[idx][0])
    {
        Rect rect = boundingRect(contours[idx]);
        r = rect.height ? (double)(rect.width/rect.height) : 0;
        if ((rect.width > connected.cols * .5) && /* filter from rect width */
            (r > 15) && /* filter from width:hight ratio */
            (r < 30) /* filter from width:hight ratio */
                )
        {
            mrz.push_back(rect);
            //rectangle(rgb, rect, Scalar(0, 255, 0), 1);
        }
        else
        {
            //rectangle(rgb, rect, Scalar(0, 0, 255), 1);
        }
    }
    if (2 == mrz.size() || 3 == mrz.size())
    {
       const CvRect &mrz0 = (CvRect)mrz[0];
       const CvRect &mrz1 = 3 == mrz.size() ? (CvRect)mrz[2] : (CvRect)mrz[1];
        // just assume we have found the two data strips in MRZ and combine them

        Rect max = cvMaxRect(&mrz0, &mrz1);
        max.x = max.x - 15;
        max.y = max.y - 15;
        max.height = max.height + 30 ;
        max.width = max.width + 30 ;
        rectangle(rgb, max, Scalar(255, 0, 0), 2);  // draw the MRZ
        //putText(rgb, "[ MRZ DETECTED ! ]", Point2f(max.x-10,max.y-10), FONT_HERSHEY_PLAIN, 2,  Scalar(255, 0, 0));
        Mat ROI(im_bw, max);

        Mat croppedImage;

// Copy the data into new matrix
        ROI.copyTo(croppedImage);
        if (path != NULL) {
            std::string bmpath = ConvertJString(env, path);
            __android_log_write(ANDROID_LOG_ERROR, "Tag", bmpath.c_str());
            imwrite(bmpath+"/MRZ.png",croppedImage);

        }

/*
 *
        vector<Point2f> mrzSrc;
        vector<Point2f> mrzDst;

        // MRZ region in our image
        mrzDst.push_back(Point2f((float)max.x, (float)max.y));
        mrzDst.push_back(Point2f((float)(max.x+max.width), (float)max.y));
        mrzDst.push_back(Point2f((float)(max.x+max.width), (float)(max.y+max.height)));
        mrzDst.push_back(Point2f((float)max.x, (float)(max.y+max.height)));

        // MRZ in our template
        mrzSrc.push_back(Point2f(0.23f, 9.3f));
        mrzSrc.push_back(Point2f(18.0f, 9.3f));
        mrzSrc.push_back(Point2f(18.0f, 10.9f));
        mrzSrc.push_back(Point2f(0.23f, 10.9f));

        // find the transformation
        Mat t = getPerspectiveTransform(mrzSrc, mrzDst);*/
/*
        // photo region in our template
        vector<Point2f> photoSrc;
        photoSrc.push_back(Point2f(0.0f, 0.0f));
        photoSrc.push_back(Point2f(5.66f, 0.0f));
        photoSrc.push_back(Point2f(5.66f, 7.16f));
        photoSrc.push_back(Point2f(0.0f, 7.16f));

        // surname region in our template
        vector<Point2f> surnameSrc;
        surnameSrc.push_back(Point2f(6.4f, 0.7f));
        surnameSrc.push_back(Point2f(8.96f, 0.7f));
        surnameSrc.push_back(Point2f(8.96f, 1.2f));
        surnameSrc.push_back(Point2f(6.4f, 1.2f));

        vector<Point2f> photoDst(4);
        vector<Point2f> surnameDst(4);

        // map the regions from our template to image
        perspectiveTransform(photoSrc, photoDst, t);
        perspectiveTransform(surnameSrc, surnameDst, t);
        // draw the mapped regions
        for (int i = 0; i < 4; i++)
        {
            line(rgb, photoDst[i], photoDst[(i+1)%4], Scalar(0,128,255), 2);
        }
        for (int i = 0; i < 4; i++)
        {
            line(rgb, surnameDst[i], surnameDst[(i+1)%4], Scalar(0,128,255), 2);
        }*/
    }

}
  void JNICALL Java_tritux_opencvnative_DetectActivity_detectMRZthersode(JNIEnv *env, jobject instance,
                                                          jlong addrRgba, jlong addrCropedImg){

    Mat rgba = *(Mat *) addrRgba;
    Mat cropedImg = *(Mat *) addrCropedImg;
    Mat gray ;

    Mat rectKernel = getStructuringElement(MORPH_RECT, Size(13, 5));
    Mat sqKernel = getStructuringElement(MORPH_RECT, Size(21, 21));

    cvtColor(rgba, gray, COLOR_RGBA2GRAY);
    //smooth the image using a 3x3 Gaussian, then apply the blackhat
    //morphological operator to find dark regions on a light background
    Mat grad;
    GaussianBlur(gray, gray, Size(9,9), 0);
    morphologyEx(gray, grad, MORPH_BLACKHAT, rectKernel);

    //compute the Scharr gradient of the blackhat image and scale the
    // result into the range [0, 255]
    Mat sobelx;
    Sobel(grad, sobelx, rgba.depth(), 1, 0);
    double minVal, maxVal;
    minMaxLoc(sobelx,&minVal, &maxVal);
    sobelx = (255 * ((sobelx - minVal) / (maxVal - minVal)));
    sobelx.convertTo(sobelx, CV_8UC1);

   // apply a closing operation using the rectangular kernel to close
  // gaps in between letters -- then apply Otsu's thresholding method
    morphologyEx(sobelx, sobelx, MORPH_CLOSE, rectKernel);
    Mat bw;
    threshold(sobelx, bw, 0, 255, THRESH_BINARY | THRESH_OTSU);

   // perform another closing operation, this time using the square
    //kernel to close gaps between lines of the MRZ, then perform a
    // series of erosions to break apart connected components
    morphologyEx(bw, bw, MORPH_CLOSE, sqKernel);
    erode(bw, bw, rectKernel);

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(bw, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    Rect bounding ;
    double maxValCoun = 0;
    int maxValIdx = 0;
    vector<vector<Point> > contours_poly( contours.size() );
    for (int i = 0; i < contours.size(); i++)
    {
        double cArea = contourArea(contours[i]);
        if (maxValCoun < cArea)
        {
            maxValCoun = cArea;
            maxValIdx = i;
            approxPolyDP(Mat(contours[i]),contours_poly[i],3,true);
            bounding = boundingRect(Mat(contours_poly[i]));
        }
    }
   // drawContours(rgba, contours, maxValIdx,  Scalar(255, 0, 0), 1);
    rectangle(rgba,bounding.br(),bounding.tl(), Scalar(0,0,255),2);
   // sobelx.convertTo(sobelx, CV_8UC1);
    //cropedImg = rgba(bounding);
}

void JNICALL Java_tritux_opencvnative_DetectActivity_detectCIN (JNIEnv *env, jobject instance,jlong addrRgba){

    Mat rgba = *(Mat *) addrRgba;
    Mat hsv ;

  /*  cvtColor(rgba, hsv, COLOR_RGB2HSV);

    Mat mask1, mask2;
    inRange(hsv, Scalar(0, 70, 50), Scalar(10, 255, 255), mask1);
    inRange(hsv, Scalar(170, 70, 50), Scalar(180, 255, 255), mask2);

    Mat mask = mask1 | mask2;*/

    //Detect
    std::vector<cv::Rect> letterBBoxes1= detectLetters(rgba);
    //Display
    for(int i=0; i< letterBBoxes1.size(); i++)
        cv::rectangle(rgba,letterBBoxes1[i],cv::Scalar(0,255,0),3,8,0);


}

void JNICALL Java_tritux_opencvnative_DetectActivity_detectTextNative(JNIEnv *env, jobject instance,jlong addrRgba){

    Mat rgb   = *(Mat *) addrRgba;
    Mat small;
    cvtColor(rgb, small, CV_BGR2GRAY);
    // morphological gradient
    Mat grad;
    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
    morphologyEx(small, grad, MORPH_GRADIENT, morphKernel);
    // binarize
    Mat bw;
    threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);
    // connect horizontally oriented regions
    Mat connected;
    morphKernel = getStructuringElement(MORPH_RECT, Size(9,1));
    morphologyEx(bw, connected, MORPH_CLOSE, morphKernel);
    // find contours
    Mat mask = Mat::zeros(bw.size(), CV_8UC1);
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(connected, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    // filter contours
    for(int idx = 0; idx >= 0; idx = hierarchy[idx][0])
    {
        Rect rect = boundingRect(contours[idx]);
        Mat maskROI(mask, rect);
        maskROI = Scalar(0, 0, 0);
        // fill the contour
        drawContours(mask, contours, idx, Scalar(255, 255, 255), CV_FILLED);
        // ratio of non-zero pixels in the filled region
        double r = (double)countNonZero(maskROI)/(rect.width*rect.height);

        if (r > .20 /* assume at least 45% of the area is filled if it contains text */
            &&
            (rect.height > 8 && rect.width > 8) /* constraints on region size */
            /* these two conditions alone are not very robust. better to use something
            like the number of significant peaks in a horizontal projection as a third condition */
                )
        {
            rectangle(rgb, rect, Scalar(0, 255, 255), 1);
        }
    }

}

void JNICALL Java_tritux_opencvnative_DetectActivity_detectBlackTextNative(JNIEnv *env, jobject instance,jlong addrRgba) {

    //detect black text
    Mat rgb = *(Mat *) addrRgba;
    Mat black;
    Mat hsv;
    cvtColor(rgb, hsv, COLOR_RGB2HSV);
    inRange(hsv, Scalar(0, 0, 0), Scalar(179, 50, 100), black);
    vector<vector<Point> > contoursBlack;
    Mat maskBlack = black;
    vector<Vec4i> hierarchy;

    findContours(maskBlack, contoursBlack, hierarchy, RETR_CCOMP, CHAIN_APPROX_TC89_KCOS);
    int num = (int)contoursBlack.size();

        fillPoly(maskBlack, contoursBlack, Scalar(255, 255, 255));

      Rect imgContour = biggestCountour(contoursBlack);
     rectangle(rgb, imgContour.br(), imgContour.tl(), Scalar(0, 0, 255), 2);



    for (int i = 0; i < contoursBlack.size(); i++) {

        Rect rect = boundingRect(contoursBlack[i]);
        Mat maskROI = Mat(maskBlack, rect);
        maskROI.setTo(Scalar(0, 0, 0));
        drawContours(maskBlack, contoursBlack, i, Scalar(255, 255, 255), FILLED);
        // ratio of non-zero pixels in the filled region
        double r = (double) countNonZero(maskROI) / (rect.width * rect.height);

        if (r > .45
            &&
            (rect.height > 5 && rect.width > 20)
                ) {
            rectangle(rgb, rect,  Scalar(0, 255, 0), 2);
        }
    }
}
void JNICALL Java_tritux_opencvnative_DetectActivity_detectCardContourNative(JNIEnv *env, jobject instance,jlong addrRgba){

    Mat rgb = *(Mat *) addrRgba;
    int largest_area=0;
    int largest_contour_index=0;
    Rect bounding_rect;

    Mat thr;
    cvtColor( rgb, thr, COLOR_RGBA2GRAY ); //Convert to gray
    threshold( thr, thr, 125, 255, THRESH_BINARY ); //Threshold the gray

    vector<vector<Point> > contours; // Vector for storing contours

    findContours( thr, contours, RETR_CCOMP, CHAIN_APPROX_SIMPLE ); // Find the contours in the image

    for( size_t i = 0; i< contours.size(); i++ ) // iterate through each contour.
    {
        double area = contourArea( contours[i] );  //  Find the area of contour

        if( area > largest_area )
        {
            largest_area = area;
            largest_contour_index = i;               //Store the index of largest contour
            bounding_rect = boundingRect( contours[i] ); // Find the bounding rectangle for biggest contour
        }
    }

    //drawContours( rgb, contours,largest_contour_index, Scalar( 0, 255, 0 ), 2 ); // Draw the largest contour using previously stored index.
    rectangle(rgb, bounding_rect, Scalar(0, 255, 0), 1);

}

void JNICALL Java_tritux_opencvnative_DetectActivity_textdetectionopencv(JNIEnv *env, jobject instance,jlong addrRgba){


    //Ptr<ERFilter> er;
    //Ptr<ERFilter> er_filter1 = createERFilterNM1(loadClassifierNM1("trained_classifierNM1.xml"),16,0.00015f,0.13f,0.2f,true,0.1f);
}
}