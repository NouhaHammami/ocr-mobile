package tritux.opencvnative.mrz;

import android.util.Log;

/**
 * Created by Tritux on 25/04/2017.
 */

public class ParsingMRZ {

    String mrzTxt;
    String surName, givenName, passportNbr, nationality, dateOfBirth, sex, expirationDate, identityNbr;

    public ParsingMRZ(String mrzTxt) {
        this.mrzTxt = mrzTxt;
    }

    public void parseMRZ(String mrzTxt){
        final String row1 = mrzTxt.substring(0,45);
        final String row2 = mrzTxt.substring(45);

        setName(parsingName(row1));
        passportNbr = parsingDocNumber(row2);
        nationality = parsingNationality(row2);
        dateOfBirth = parsingDateOfBirth(row2);
        sex = parsingSex(row2);
        expirationDate = parsingDateOfExpiration(row2);
        identityNbr = parsingidentityNbr(row2);
    }


    public String[] parsingName(String row){

        final String typeandsurname = row.split("<<")[0];
        final String surname = typeandsurname.substring(5);
        final String givenNames = row.split("<<")[1];
        Log.e("## parsing",surname +" ## "+givenNames );
        return new String[]{surname, givenNames};
    }

    public String parsingDocNumber(String row){

        String docNumber = row.substring(0,9);
       /* if(docNumber.endsWith("<")){
            return docNumber.split("<")[0];
        }*/
        Log.e("## docNumber",docNumber);
        return docNumber;

    }

    public String parsingNationality(String range){

        String nationality = range.substring(10,13);
        Log.e("## nationality",nationality);
        return nationality;
    }

    public String parsingDateOfBirth(String range){

        String dateofbirth = range.substring(13,19);
        //String ddmmyy = new StringBuilder(dateofbirth).reverse().toString();
        String monthName;
        String month = dateofbirth.substring(2,4);
        switch (month){
            case "01" : monthName = "Janvier";
                break;
            case "02" : monthName = "Février";
                break;
            case "03" : monthName = "Mars";
                break;
            case "04" : monthName = "Avril";
                break;
            case "05" : monthName = "Mai";
                break;
            case "06" : monthName = "Juin";
                break;
            case "07" : monthName = "Juillet";
                break;
            case "08" : monthName = "Aout";
                break;
            case "09" : monthName = "Septembre";
                break;
            case "10" : monthName = "Octobre";
                break;
            case "11" : monthName = "Novembre";
                break;
            case "12" : monthName = "Décembre";
                break;

            default: monthName = month;

        }
        dateofbirth = dateofbirth.substring(4,6) +" "+ monthName +" "+ dateofbirth.substring(0,2);
        Log.e("## dateofbirth",dateofbirth);
        return dateofbirth;

    }

    public String parsingSex(String row){
        String sex = row.substring(20,21);
        Log.e("##", "parsingSex: "+sex );
        if(sex.equals("M")){
            return "Homme";
        }else
        if(sex.equals("F"))
            return "femme";
        else return "bad lecture";
    }

    private String parsingDateOfExpiration(String row2) {

        String dateExp = row2.substring(21,27);
        String monthName;
        String month = dateExp.substring(2,4);

        switch (month){
            case "01" : monthName = "Janvier";
                break;
            case "02" : monthName = "Février";
                break;
            case "03" : monthName = "Mars";
                break;
            case "04" : monthName = "Avril";
                break;
            case "05" : monthName = "Mai";
                break;
            case "06" : monthName = "Juin";
                break;
            case "07" : monthName = "Juillet";
                break;
            case "08" : monthName = "Aout";
                break;
            case "09" : monthName = "Septembre";
                break;
            case "10" : monthName = "Octobre";
                break;
            case "11" : monthName = "Novembre";
                break;
            case "12" : monthName = "Décembre";
                break;

            default: monthName = month;

        }
        dateExp = dateExp.substring(4,6) +" "+ monthName +" "+ dateExp.substring(0,2);
        Log.e("## dateExp",dateExp);
        return dateExp;

    }

    public String parsingidentityNbr(String row){

        Log.e("##", "idnbr " +row.substring(28,42));
        return row.substring(28,42);
    }

    protected final void setName(String[] name) {
        surName = name[0];
        givenName = name[1];
    }

    @Override
    public String toString() {
       return("Prénom : "+surName +"\n"
                + "Nom : "+givenName + "\n"
                + "Passeport ID : "+passportNbr + "\n"
                + "Nationalité : "+nationality + "\n"
                + "Date de naissance : "+dateOfBirth + "\n"
                + "Sex : "+sex + "\n"
                + "Date d expiration : "+expirationDate + "\n"
                + "Nbr d identité : "+identityNbr);
    }
}
