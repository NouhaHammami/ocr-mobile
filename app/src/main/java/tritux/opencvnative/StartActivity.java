package tritux.opencvnative;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Tritux on 08/04/2017.
 */

public class StartActivity extends Activity implements View.OnClickListener {

    Button cinBtnR, cinBtnV, mrzBtn;
    public static int DETECTCINR = 0;
    public static int DETECTCINV = 1;
    public static int DETECTMRZ = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        cinBtnR = (Button) findViewById(R.id.buttonCINR);
        cinBtnV = (Button) findViewById(R.id.buttonCINV);
        mrzBtn = (Button) findViewById(R.id.buttonMRZ);

        cinBtnR.setOnClickListener(this);
        cinBtnV.setOnClickListener(this);
        mrzBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        Intent scanIntent = new Intent(StartActivity.this, DetectActivity.class);

      switch (view.getId()){
          case R.id.buttonCINR :
              scanIntent.putExtra("DETECTMODE",DETECTCINR);
              break;
          case R.id.buttonCINV :
              scanIntent.putExtra("DETECTMODE",DETECTCINV);
              break;
          case R.id.buttonMRZ :
              scanIntent.putExtra("DETECTMODE",DETECTMRZ);
              break;
      }
        startActivity(scanIntent);
    }
}
