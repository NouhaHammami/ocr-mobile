package tritux.opencvnative;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class OcrActivity extends Activity {
    Ocr mTessOCR;
    ProgressDialog mProgressDialog;
    TextView txtViewTitre;
    ImageView imageQualityText;
    private static final String TAG = "OcrActivity";

    static {
        if(!OpenCVLoader.initDebug()){
            Log.d(TAG, "OpenCV not loaded");
        } else {
            Log.d(TAG, "OpenCV loaded");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocr);
        mTessOCR = new Ocr(this,"ara");
        mProgressDialog=new ProgressDialog(getApplicationContext());
        Bitmap titreImage = BitmapFactory.decodeResource(getResources(),
                R.mipmap.test);
        txtViewTitre=(TextView) findViewById(R.id.txtViewTitre);


        ///open cv
        imageQualityText=(ImageView) findViewById(R.id.image_quality_text) ;
        ///craate mutable bitmap from drawable
        // Create a bitmap of the same size
        Bitmap newBmp = Bitmap.createBitmap(titreImage.getWidth(), titreImage.getHeight(), Bitmap.Config.ARGB_4444);
// Create a canvas  for new bitmap
        Canvas c = new Canvas(newBmp);

// Draw your old bitmap on it.
        c.drawBitmap(titreImage, 0, 0, new Paint());

        imageQualityText.setImageBitmap(newBmp);
        openCvChangeQualityImage(titreImage);
        //doOCR(titreImage);



    }

    private void doOCR (final Bitmap bitmap) {
       /* if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getApplicationContext(), "Processing",
                    "Doing OCR...", true);
        } else {
            mProgressDialog.show();
        }*/
        new Thread(new Runnable() {
            public void run() {
                final String srcText = mTessOCR.getOCRResult(bitmap);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        if (srcText != null && !srcText.equals("")) {
                            //srcText contiene el texto reconocido
                            txtViewTitre.setText(srcText);
                            Log.i("result","mot"+srcText);
                        }
                        mTessOCR.onDestroy();
                       // mProgressDialog.dismiss();
                    }
                });
            }
        }).start();
    }

    public void openCvChangeQualityImage(Bitmap bitmap){
        //get imageBitmap from path
        //bitmap = BitmapFactory.decodeFile(imageName);
        Mat imageMat = new Mat();
        org.opencv.android.Utils.bitmapToMat(bitmap, imageMat);

        Imgproc.cvtColor(imageMat, imageMat, Imgproc.COLOR_BGR2GRAY);
        // 1) Apply gaussian blur to remove noise
        //Imgproc.GaussianBlur(imageMat, imageMat, new Size(9,9), 0);
// 2) AdaptiveThreshold -> classify as either black or white
        //Imgproc.adaptiveThreshold(imageMat, imageMat, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 5, 2);

// 3) Invert the image -> so most of the image is black
      //  Core.bitwise_not(imageMat, imageMat);

// 4) Dilate -> fill the image using the MORPH_DILATE
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_DILATE, new Size(3,3), new Point(1,1));
        Imgproc.dilate(imageMat, imageMat, kernel);

        org.opencv.android.Utils.matToBitmap(imageMat, bitmap);
        imageQualityText.setImageBitmap(bitmap);
        doOCR(bitmap);
       /* ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream1);
        byteArray = stream1.toByteArray();*/
    }
}
