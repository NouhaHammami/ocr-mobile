package tritux.opencvnative;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import static com.googlecode.tesseract.android.TessBaseAPI.PageSegMode.PSM_SINGLE_BLOCK;

/**
 * Created by Tritux on 11/04/2017.
 */

public class Ocr extends AsyncTask{
    private final TessBaseAPI mTess;
    private String datapath;

    public Ocr(Context context, String language) {

        datapath = Environment.getExternalStorageDirectory() + "/tesseract";

        File dir = new File(datapath + "/tessdata/");
        if (!dir.exists()) {
            Log.d("mylog", "in file doesn't exist");
            dir.mkdirs();
            copyFile(context,language);
        }
        mTess = new TessBaseAPI();
        mTess.init(datapath, language);

        //String datapath = context.getFilesDir() + "/tesseract/";
        // mTess.init(datapath, language);
    /*    File root = new File(Environment.getExternalStorageDirectory().getPath() + "/tesseract");
        if(!root.exists()){
            root.mkdir();
        }
        File dest = new File(root.getPath() + "/" + "tessdata"); //Costumer folder
        if(!dest.exists()){
            dest.mkdir();
        }
        String datapath = Environment.getExternalStorageDirectory().getPath()+ "/tesseract/";
        mTess.init(datapath, language);*/
        // Environment.getExternalStorageDirectory().getPath() + "/XpressConnect"
    }

    public String getOCRResult(Bitmap bitmap) {
        mTess.setImage(bitmap);
        mTess.setPageSegMode(PSM_SINGLE_BLOCK);
        return mTess.getUTF8Text();
    }

    private void copyFile(Context context, String language) {
        AssetManager assetManager = context.getAssets();
        try {
            InputStream in = assetManager.open(language.split("\\+")[0]+".traineddata");
            InputStream numa = assetManager.open("numa.traineddata");
            OutputStream out = new FileOutputStream(datapath + "/tessdata/" + language.split("\\+")[0]+".traineddata");
            byte[] buffer = new byte[1024];
            int read = in.read(buffer);
            while (read != -1) {
                out.write(buffer, 0, read);
                read = in.read(buffer);
            }

            OutputStream outnuma = new FileOutputStream(datapath + "/tessdata/"+"numa.traineddata");
            byte[] buffernuma = new byte[1024];
            int readuma = numa.read(buffernuma);
            while (readuma != -1) {
                outnuma.write(buffernuma, 0, readuma);
                readuma = numa.read(buffernuma);
            }
        } catch (Exception e) {
            Log.d("mylog", "couldn't copy with the following error : "+e.toString());
        }
    }

    public void onDestroy() {
        if (mTess != null) mTess.end();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        return null;
    }
}