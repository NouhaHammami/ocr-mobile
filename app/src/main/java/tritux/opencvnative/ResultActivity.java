package tritux.opencvnative;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import tritux.opencvnative.mrz.MrzParser;
import tritux.opencvnative.mrz.MrzRecord;
import tritux.opencvnative.mrz.ParsingMRZ;

/**
 * Created by Tritux on 08/04/2017.
 */

public class ResultActivity extends Activity {

    ImageView numCin, nomCin, prenomCin, prenom2Cin, birthdayCin, cityCin ;
    TextView  infoTitle, txtNumCin, txtNomCin, txtPrenomCin, txtPrenomCin2, txtBirthday, txtCity;
    TextView nom, prenom;
    Ocr mTessOCRAra, mTessOCRFra;
    ProgressDialog progressDialog;
    /** Languages for which Cube data is available. */
    static final String[] CUBE_SUPPORTED_LANGUAGES = {
            "ara", // Arabic
            "fra",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        findViewsByIds();
        progressDialog = new ProgressDialog(getApplicationContext(),ProgressDialog.STYLE_SPINNER);

        if(getIntent().getIntExtra("DETECTMODE", -1) == StartActivity.DETECTMRZ) {
            byte[] mrzArray = getIntent().getByteArrayExtra("MRZ");
            final Bitmap mrzBitmap = BitmapFactory.decodeByteArray(mrzArray, 0, mrzArray.length);
                prenom2Cin.setImageBitmap(mrzBitmap);
            infoTitle.setText(getResources().getString(R.string.MRZ));
            txtNumCin.setVisibility(View.GONE);
            txtNomCin.setVisibility(View.GONE);
            txtPrenomCin.setVisibility(View.GONE);
            txtBirthday.setVisibility(View.GONE);
            txtCity.setVisibility(View.VISIBLE);
            new AsyncTask<Object, Object, Boolean>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                     progressDialog = ProgressDialog.show(ResultActivity.this,"Traitement en crous ...","");
                }

                @Override
                protected Boolean doInBackground(Object... voids) {
                    doOCRforMRZ(mrzBitmap);
                    return true;
                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);
                    progressDialog.dismiss();
                }
            }.execute();


        }else
        if(getIntent().getIntExtra("DETECTMODE", -1) == StartActivity.DETECTCINR) {
            new AsyncTask<Object, Object, Boolean>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog = ProgressDialog.show(ResultActivity.this,"Traitement en crous ...","");
                }

                @Override
                protected Boolean doInBackground(Object... objects) {
                    setBitmapRectoFromIntent();
                    return true;
                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);
                    progressDialog.dismiss();
                }
            }.execute();
        }else
        if(getIntent().getIntExtra("DETECTMODE", -1) == StartActivity.DETECTCINV){// VERSO
            new AsyncTask<Object, Object, Boolean>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog = ProgressDialog.show(ResultActivity.this,"Traitement en crous ...","");
                }

                @Override
                protected Boolean doInBackground(Object... objects) {
                    setBitmapVersoFromIntent();
                    return true;
                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);
                    progressDialog.dismiss();
                }
            }.execute();

        }

    }

    public void setBitmapVersoFromIntent(){

        //addr cin
        byte[] addrCinArray = getIntent().getByteArrayExtra("cinVerso");
        Bitmap bmpAdr = BitmapFactory.decodeByteArray(addrCinArray, 0, addrCinArray.length);
        //txtPrenomCin.setText("Adresse");
        prenom2Cin.setImageBitmap(bmpAdr);

        //doOCRforVerso(bmpAdr);
        txtNumCin.setVisibility(View.GONE);
        txtNomCin.setVisibility(View.GONE);
        txtBirthday.setVisibility(View.GONE);
       // txtCity.setVisibility(View.GONE);

    }

    public void setBitmapRectoFromIntent(){

        List<Bitmap> bitmapList = new LinkedList<>();
        //cin recto
        byte[] cinRectoArray = getIntent().getByteArrayExtra("cinRecto");
        Bitmap bmpCinRecto = BitmapFactory.decodeByteArray(cinRectoArray, 0, cinRectoArray.length);
       // bitmaps.add(bmpCinRecto);
       // Bitmap bmpNumCinRecto = BitmapFactory.decodeByteArray(numCINArray, 0, numCINArray.length);
        prenom2Cin.setImageBitmap(bmpCinRecto);

        /************************************************************************
        //num cin
       */ byte[] numCinArray = getIntent().getByteArrayExtra("numCin");
        Bitmap bmpNum = BitmapFactory.decodeByteArray(numCinArray, 0, numCinArray.length);
        //numCin.setImageBitmap(bmpNum);
       // bitmapList.add(bmpNum);
       /* //nom cin
        byte[] nomCinArray = getIntent().getByteArrayExtra("nomCin");
        Bitmap bmpNom = BitmapFactory.decodeByteArray(nomCinArray, 0, nomCinArray.length);
        nomCin.setImageBitmap(bmpNom);
        bitmapList.add(bmpNom);
        //prenom cin
        byte[] prenomCinArray = getIntent().getByteArrayExtra("prenomCin");
        Bitmap bmpPrenom = BitmapFactory.decodeByteArray(prenomCinArray, 0, prenomCinArray.length);
        prenomCin.setImageBitmap(bmpPrenom);
        bitmapList.add(bmpPrenom);
        //prenom2 cin
        byte[] prenom2CinArray = getIntent().getByteArrayExtra("prenom2Cin");
        Bitmap bmpPrenom2 = BitmapFactory.decodeByteArray(prenom2CinArray, 0, prenom2CinArray.length);
        prenom2Cin.setImageBitmap(bmpPrenom2);
        bitmapList.add(bmpPrenom2);
        //birthday cin
        byte[] birthdayCinArray = getIntent().getByteArrayExtra("birthdayCin");
        Bitmap bmpBirthday = BitmapFactory.decodeByteArray(birthdayCinArray, 0, birthdayCinArray.length);
        birthdayCin.setImageBitmap(bmpBirthday);
        bitmapList.add(bmpBirthday);
        //city cin
        byte[] cityCinArray = getIntent().getByteArrayExtra("cityCin");
        Bitmap bmpCity = BitmapFactory.decodeByteArray(cityCinArray, 0, cityCinArray.length);
        cityCin.setImageBitmap(bmpCity);
        bitmapList.add(bmpCity);
*****************************************************************************************/
        //doOCR(bmpCinRecto,bmpNum);
    }

    private void findViewsByIds(){

        numCin = (ImageView) findViewById(R.id.imageViewNumCin);
        nomCin = (ImageView) findViewById(R.id.imageViewNom);
        prenomCin = (ImageView) findViewById(R.id.imageViewPrenom);
        prenom2Cin = (ImageView) findViewById(R.id.imageViewPrenom2);
        birthdayCin = (ImageView) findViewById(R.id.imageViewBirthday);
        cityCin = (ImageView) findViewById(R.id.imageViewCity);

        infoTitle = (TextView) findViewById(R.id.textViewInfoCin);
        txtNumCin = (TextView) findViewById(R.id.textViewNumCin);
        txtNomCin = (TextView) findViewById(R.id.textViewNom);
        txtPrenomCin = (TextView) findViewById(R.id.textViewPrenom);
        txtPrenomCin2 = (TextView) findViewById(R.id.textViewPrenom2);
        txtBirthday = (TextView) findViewById(R.id.textViewBirthday);
        txtCity = (TextView) findViewById(R.id.textViewCity);

        ////////************************************//////////
        nom = (TextView) findViewById(R.id.nom);
        prenom = (TextView) findViewById(R.id.prenom);

    }

    private void doOCRforMRZ(final Bitmap bitmap){

        mTessOCRFra = new Ocr(this,"fra");

        new Thread(new Runnable() {
            String mrzTxt ;
            @Override
            public void run() {

                mrzTxt = mTessOCRFra.getOCRResult(bitmap);

                final String l1 = mrzTxt.substring(0,44);
                final String l2 = mrzTxt.substring(45);

                Log.e("mrz parsed",l1 +" | "+l2+" \n"+l1+l2);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtCity.setText(mrzTxt);
                        Log.i("result","mrzTxt "+mrzTxt);

                        /*MrzRecord record = MrzParser.parse(l1+"\n"+l2);
                            txtCity.setText(record.toString());*/

                        ParsingMRZ record = new  ParsingMRZ(mrzTxt);
                        record.parseMRZ(mrzTxt);
                        txtCity.setText(record.toString());
                        mTessOCRFra.onDestroy();
                    }
                });
            }
        }).start();


    }
    private void doOCRforVerso(final Bitmap bitmap){

        mTessOCRAra = new Ocr(this,"ara+numa");

        new Thread(new Runnable() {
            String adresseTxt ;
            @Override
            public void run() {

                adresseTxt = mTessOCRAra.getOCRResult(bitmap);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtCity.setText(adresseTxt);
                        Log.i("result","adresseTxt "+adresseTxt);

                        mTessOCRAra.onDestroy();
                    }
                });
            }
        }).start();


    }

    private void doOCR (final Bitmap bitmaps, final Bitmap bitmapnum) {
        mTessOCRAra = new Ocr(this,"ara");
        mTessOCRFra = new Ocr(this,"fra");
      /*  if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getApplicationContext(), "Processing",
                    "Doing OCR...", true);
        } else {
            mProgressDialog.show();
        }*/
        new Thread(new Runnable() {
            String numTxt, nomTxt, prenomTxt, prenom2Txt, birthTxt, cityTxt;
            public void run() {
                    numTxt = mTessOCRFra.getOCRResult(bitmapnum);
                   /* nomTxt = mTessOCRAra.getOCRResult(bitmaps.get(1));
                    prenomTxt = mTessOCRAra.getOCRResult(bitmaps.get(2));
                    prenom2Txt = mTessOCRAra.getOCRResult(bitmaps.get(3));
                    birthTxt = mTessOCRAra.getOCRResult(bitmaps.get(4));*/
                    cityTxt = mTessOCRAra.getOCRResult(bitmaps);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                            //srcText contiene el texto reconocido
                           /* txtNumCin.setText(numTxt);
                            txtNomCin.setText(nomTxt);
                            txtPrenomCin.setText(prenomTxt);
                            txtPrenomCin2.setText(prenom2Txt);
                            txtBirthday.setText(birthTxt);*/
                            txtCity.setText(numTxt+"\n"+cityTxt);
                            Log.i("result","mot"+cityTxt);

                        mTessOCRAra.onDestroy();
                        //mTessOCRFra.onDestroy();
                        // mProgressDialog.dismiss();
                    }
                });
            }
        }).start();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/DetectApp/MRZ.png");
        if (imgFile.exists())
            imgFile.delete();
    }
}
