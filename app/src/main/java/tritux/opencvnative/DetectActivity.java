package tritux.opencvnative;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static java.util.Collections.swap;
import static org.opencv.core.Core.FONT_HERSHEY_SIMPLEX;
import static org.opencv.core.Core.subtract;
import static org.opencv.core.CvType.CV_32SC2;
import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.Mat.zeros;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;
import static org.opencv.imgproc.Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C;
import static org.opencv.imgproc.Imgproc.ADAPTIVE_THRESH_MEAN_C;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2GRAY;
import static org.opencv.imgproc.Imgproc.COLOR_RGB2HSV;
import static org.opencv.imgproc.Imgproc.COLOR_RGBA2GRAY;
import static org.opencv.imgproc.Imgproc.GaussianBlur;
import static org.opencv.imgproc.Imgproc.INTER_CUBIC;
import static org.opencv.imgproc.Imgproc.MORPH_CLOSE;
import static org.opencv.imgproc.Imgproc.MORPH_CROSS;
import static org.opencv.imgproc.Imgproc.MORPH_ELLIPSE;
import static org.opencv.imgproc.Imgproc.MORPH_GRADIENT;
import static org.opencv.imgproc.Imgproc.MORPH_OPEN;
import static org.opencv.imgproc.Imgproc.MORPH_RECT;
import static org.opencv.imgproc.Imgproc.RETR_CCOMP;
import static org.opencv.imgproc.Imgproc.Sobel;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY_INV;
import static org.opencv.imgproc.Imgproc.THRESH_MASK;
import static org.opencv.imgproc.Imgproc.THRESH_OTSU;
import static org.opencv.imgproc.Imgproc.adaptiveThreshold;
import static org.opencv.imgproc.Imgproc.boundingRect;
import static org.opencv.imgproc.Imgproc.contourArea;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.drawContours;
import static org.opencv.imgproc.Imgproc.erode;
import static org.opencv.imgproc.Imgproc.findContours;
import static org.opencv.imgproc.Imgproc.getPerspectiveTransform;
import static org.opencv.imgproc.Imgproc.getStructuringElement;
import static org.opencv.imgproc.Imgproc.morphologyEx;
import static org.opencv.imgproc.Imgproc.polylines;
import static org.opencv.imgproc.Imgproc.putText;
import static org.opencv.imgproc.Imgproc.pyrDown;
import static org.opencv.imgproc.Imgproc.rectangle;
import static org.opencv.imgproc.Imgproc.threshold;
import static org.opencv.imgproc.Imgproc.warpPerspective;

/**
 * Created by Tritux on 05/04/2017.
 */

public class DetectActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2, View.OnTouchListener {

    private static final String TAG = "MRZ::Activity";
    private CameraBridgeViewBase _cameraBridgeViewBase;
    Mat mRgba ;
    Mat mGray;
    Mat MRZRoi;
    Bitmap numCinBitmap, nomCinBitmap, prenomCinBitmap, prenom2CinBitmap, birthdayCinBitmap, cityCinBitmap, addCinBitmap;
    Bitmap mrzBitmap,cinBitmap,cinBitmapR,cinBitmapV;
    String msgDetect = null ;
    String path;
    int detectMode = -1;
    Scalar CONTOUR_WHITE = new Scalar(255,255,255);
    Scalar CONTOUR_BLUE = new Scalar(0,0,255);
    Rect numCin = new Rect(), nomCin = new Rect(), prenom1Cin = new Rect(), prenom2Cin = new Rect(),
            bithdayCin = new Rect(), cityCin = new Rect(), adressCinV = new Rect();
    Rect cinRect = new Rect();
    Bitmap userIcon, fingerPrintIcon;
    RelativeLayout rl;
    ImageView iv ;
    Rect imgCinRecto = new Rect();
    Rect imgCinVerso = new Rect();
    Rect textCinRecto = new Rect();
    Rect numCinRecto = new Rect();
    RotatedRect rotatedRectRecto = new RotatedRect();
    ImageButton scanBtn;

    private BaseLoaderCallback _baseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    // Load ndk built module, as specified in moduleName in build.gradle
                    // after opencv initialization
                    System.loadLibrary("native-lib");
                    _cameraBridgeViewBase.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        // Permissions for Android 6+
        ActivityCompat.requestPermissions(DetectActivity.this,
                new String[]{Manifest.permission.CAMERA},
                1);

        _cameraBridgeViewBase = (CameraBridgeViewBase) findViewById(R.id.main_surface);
        scanBtn = (ImageButton) findViewById(R.id.scanButton);
        _cameraBridgeViewBase.setVisibility(SurfaceView.VISIBLE);
        _cameraBridgeViewBase.setCvCameraViewListener(this);

        _cameraBridgeViewBase.setOnTouchListener(this);
        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detectMode == StartActivity.DETECTCINR) {
                    cinBitmapR = setBitmapCIN(textCinRecto, cinBitmapR, mRgba);
                    numCinBitmap = setBitmapCIN(numCinRecto, numCinBitmap, mRgba);
                    /*nomCinBitmap = setBitmapCIN(nomCin, nomCinBitmap, mRgba);
                    prenomCinBitmap = setBitmapCIN(prenom1Cin, prenomCinBitmap, mRgba);
                    prenom2CinBitmap = setBitmapCIN(prenom2Cin, prenom2CinBitmap, mRgba);
                    birthdayCinBitmap = setBitmapCIN(bithdayCin, birthdayCinBitmap, mRgba);
                    cityCinBitmap = setBitmapCIN(cityCin, cityCinBitmap, mRgba);

                        //cinBitmapR != null ? cinBitmapR : cinBitmapV);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                        ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                        ByteArrayOutputStream stream3 = new ByteArrayOutputStream();
                        ByteArrayOutputStream stream4 = new ByteArrayOutputStream();
                        ByteArrayOutputStream stream5 = new ByteArrayOutputStream();*/
                        ByteArrayOutputStream stream6 = new ByteArrayOutputStream();
                    ByteArrayOutputStream streamRecto = new ByteArrayOutputStream();
                        Intent resultIntent = new Intent(DetectActivity.this, ResultActivity.class);

                    cinBitmapR.compress(Bitmap.CompressFormat.PNG, 100, streamRecto);
                    numCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream6);
                                /*prenomCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                                prenom2CinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream3);
                                birthdayCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream4);
                                cityCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream5);

                                byte[] numCinArray = stream.toByteArray();
                                byte[] nomCinArray = stream1.toByteArray();
                                byte[] prenomCinArray = stream2.toByteArray();
                                byte[] prenom2CinArray = stream3.toByteArray();
                                byte[] birthdayCinArray = stream4.toByteArray();
                                byte[] cityCinArray = stream5.toByteArray();
*/
                                //resultIntent.putExtra("cinRecto",streamRecto.toByteArray());
                                resultIntent.putExtra("numCin", stream6.toByteArray());
                               /* resultIntent.putExtra("nomCin", nomCinArray);
                                resultIntent.putExtra("prenomCin", prenomCinArray);
                                resultIntent.putExtra("prenom2Cin", prenom2CinArray);
                                resultIntent.putExtra("birthdayCin", birthdayCinArray);
                                resultIntent.putExtra("cityCin", cityCinArray);*/

                                resultIntent.putExtra("cinRecto", streamRecto.toByteArray());
                                 resultIntent.putExtra("DETECTMODE",detectMode);
                                startActivity(resultIntent);
                                finish();

                }else{
                    if (detectMode == StartActivity.DETECTCINV) {

                        cinBitmapV = setBitmapCIN(adressCinV, cinBitmapV, mRgba);

                        if ( cinBitmapV != null) {
                            ByteArrayOutputStream stream12 = new ByteArrayOutputStream();
                            Intent resultIntent1 = new Intent(DetectActivity.this, ResultActivity.class);

                            cinBitmapV.compress(Bitmap.CompressFormat.PNG, 100, stream12);
                            byte[] cinRArray = stream12.toByteArray();
                            resultIntent1.putExtra("cinVerso", cinRArray);
                            resultIntent1.putExtra("DETECTMODE", detectMode);
                            startActivity(resultIntent1);
                            finish();
                        }
                    }
                }
            }
        });

        detectMode = getIntent().getIntExtra("DETECTMODE",-1);

        File root = new File(Environment.getExternalStorageDirectory().getPath() + "/DetectApp");
        if(!root.exists()){
            root.mkdir();
            path = root.getPath();
            Log.e("PATH", path);
        }else
            path = root.getPath();

         rl = (RelativeLayout) findViewById(R.id.activity_main);
         iv = new ImageView(this);

        userIcon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_account_grey600_36dp);
        fingerPrintIcon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_fingerprint_grey600_36dp);

    }

    @Override
    public void onPause() {
        super.onPause();
        disableCamera();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, _baseLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            _baseLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(DetectActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void onDestroy() {
        super.onDestroy();
        disableCamera();
    }

    public void disableCamera() {
        if (_cameraBridgeViewBase != null)
            _cameraBridgeViewBase.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
        MRZRoi = new Mat();
    }

    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
        MRZRoi.release();
    }


    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        //outputImg = Bitmap.createBitmap(mRgba.cols(), mRgba.rows(), Bitmap.Config.ARGB_8888);
        /************detectMRZ(mRgba.getNativeObjAddr());*/
        //if(!detected) {
           // detectMRZthersode(mRgba.getNativeObjAddr(), cropedImg.getNativeObjAddr());
           // Utils.matToBitmap(cropedImg, outputImg);
          //  if (outputImg != null) {
          //      mrzImg.setImageBitmap(outputImg);
          //      detected = true;
           // }
       // }
       // detectCIN(mRgba.getNativeObjAddr());

       // return  detectCIN(mRgba);

        //detectTextNative(mRgba.getNativeObjAddr());
        if(detectMode == StartActivity.DETECTMRZ) {
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/DetectApp/MRZ.png");
            if(file.exists()){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Bitmap MRZBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                MRZBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] MRZArray = stream.toByteArray();
                Intent MRZResultIntent = new Intent(DetectActivity.this, ResultActivity.class);
                MRZResultIntent.putExtra("MRZ",MRZArray);
                MRZResultIntent.putExtra("DETECTMODE",StartActivity.DETECTMRZ);
                startActivity(MRZResultIntent);
                finish();
            }else {
                detectMRZNative(mRgba.getNativeObjAddr(),path);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        scanBtn.setVisibility(View.GONE);
                    }
                });
            }



        }
        //detectBlackTextNative(mRgba.getNativeObjAddr());
        //detectCardContour(mRgba);
        //detectCardContourNative(mRgba.getNativeObjAddr());
        else
        if(detectMode == StartActivity.DETECTCINR){
            //detectText(mRgba);
            detectRectoCIN(mRgba);
        }else{
            detectVersoCIN(mRgba);
        }

           //detectCardContourJava(mRgba);
            //detectCardContour(mRgba);



        return mRgba;
    }

    public void detectRectoCIN (Mat rgb){


        Mat gray = new Mat();
        cvtColor( rgb, gray, COLOR_RGBA2GRAY ); //Convert to gray
        Mat thr = new Mat();
        threshold( gray, thr, 125, 255, THRESH_BINARY ); //Threshold the gray
        //adaptiveThreshold(gray,thr,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,5,20);

        List<MatOfPoint> contours = new LinkedList<>(); // Vector for storing contours
        findContours( thr, contours,new Mat(), RETR_CCOMP, CHAIN_APPROX_SIMPLE ); // Find the contours in the image
         imgCinRecto = biggestCountour(contours); // Find the bounding rectangle for biggest contour
        numCinRecto = new Rect(imgCinRecto.width/3+imgCinRecto.x, imgCinRecto.height/3+imgCinRecto.y,
                imgCinRecto.width/3,imgCinRecto.height/10);
        //drawContours( rgb, contours,-1, new Scalar( 0, 255, 0 ), 2 ); // Draw the largest contour using previously stored index.
        MatOfPoint m = biggestCountousMatOfPoint(contours);

        // ********************************   create template
        int x = (rgb.width() /10) * 1;
        int y = (rgb.height() /10) * 3;
        int w = (rgb.width() /10) * 8;
        int h = (rgb.height()/10) * 6;
        Rect template = new Rect(x,y,w,h); //cin template
        rectangle(mRgba,template.tl(),template.br(),new Scalar(255,0,255),3);// draw pink template

        final Rect userIconRect = new Rect(template.width / 8 + template.x, template.height / 3 + template.y, template.width / 4, template.height / 2);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //imgIcon.setImageBitmap(userIcon);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(userIconRect.width, userIconRect.height);
                params.leftMargin = userIconRect.x +50;
                params.topMargin = userIconRect.y;
                if(iv.getParent() == null) {
                    rl.addView(iv, params);
                    iv.setImageBitmap(userIcon);
                }//else
                //((RelativeLayout)rl.getParent()).removeView(iv);
            }
        });
        ////////////////////////****************************************************** END

            rotatedRectRecto = Imgproc.minAreaRect(new MatOfPoint2f(m.toArray()));
            if (imgCinRecto.area() > 300000 && imgCinRecto.area() < 900000) {
                drawRotatedRect(rgb, rotatedRectRecto, new Scalar(0, 0, 255), 2);
                //rectangle(rgb, imgCinRecto.br(), imgCinRecto.tl(), CONTOUR_BLUE, 1); //cin contour
               // rectangle(rgb, numCinRecto.br(), numCinRecto.tl(), CONTOUR_WHITE,1); //num cin contour
                //drawCinTextContours(mRgba,imgCinRecto);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        scanBtn.setVisibility(View.VISIBLE);

                    }
                });

                    //rect to mat
          /*  Mat ROI = new Mat(rgb, userIconRect);
            Mat croppedImage = new Mat(userIconRect.x,userIconRect.y,CV_32SC2);
            ROI.copyTo(croppedImage);

            MatOfPoint mpoints = new MatOfPoint(userIconRect.tl(),new Point(userIconRect.tl().x+userIconRect.width, userIconRect.tl().y),
                    userIconRect.br(), new Point(userIconRect.tl().x,userIconRect.tl().y+userIconRect.height),
                    new Point());
            MatOfPoint2f points2f = new MatOfPoint2f(mpoints.toArray());

            RotatedRect r = Imgproc.minAreaRect(points2f);
            drawRotatedRect(rgb,r,new Scalar(255,0,0),2);*/
                    // rectangle(rgb, userIconRect.tl(), userIconRect.br(), new Scalar(255, 0, 0), 2);  // draw the template icon


                    /*else
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    iv.setVisibility(View.GONE);

                }
            });*/

       /* cinBitmap = setBitmapCIN(imgContour, cinBitmap, mRgba);

        if(cinBitmap != null){
            SaveImage(cinBitmap);
        }*/

      /*  Mat im_bw = new Mat();
        Mat roi = new Mat(im_bw, userIconRect);
        Mat croppedImage = new Mat();
        roi.copyTo(croppedImage);
        imwrite(Environment.getExternalStorageDirectory().getAbsolutePath()+"/recto.png",croppedImage);*/
        /*
        Mat imgMat = new Mat();
        Bitmap bmp32 = userIcon.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmp32, imgMat);*/
            }else
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        scanBtn.setVisibility(View.GONE);

                    }
                });

        textCinRecto = new Rect(imgCinRecto.width / 3 + imgCinRecto.x,(7* (imgCinRecto.height/15)) + imgCinRecto.y ,
                2*(imgCinRecto.width/3),5*(imgCinRecto.height/10));

    }

    public void detectVersoCIN(Mat rgb){

        Mat gray = new Mat();
        cvtColor( rgb, gray, COLOR_RGBA2GRAY ); //Convert to gray
        Mat thr = new Mat();
        threshold( gray, thr, 125, 255, THRESH_BINARY); //Threshold the gray

        List<MatOfPoint> contours = new LinkedList<>(); // Vector for storing contours
        findContours( thr, contours,new Mat(), RETR_CCOMP, CHAIN_APPROX_SIMPLE ); // Find the contours in the image
        imgCinVerso = biggestCountour(contours); // Find the bounding rectangle for biggest contour
        //drawContours( rgb, contours,largest_contour_index, Scalar( 0, 255, 0 ), 2 ); // Draw the largest contour using previously stored index.
        rectangle(rgb, imgCinVerso.br(), imgCinVerso.tl(), CONTOUR_BLUE, 1);

        // ********************************   create template VERSO
        int x = (rgb.width() /10) * 1;
        int y = (rgb.height() /10) * 2;
        int w = (rgb.width() /10) * 8;
        int h = (rgb.height()/10) * 6;
        Rect template = new Rect(x,y,w,h); //cin template
        rectangle(mRgba,template.tl(),template.br(),new Scalar(255,0,255),3);// draw pink template

        final Rect fingerPrintRect = new Rect(9*(template.width / 10) + template.x, template.height / 4 + template.y, template.width / 4, template.height / 2);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //imgIcon.setImageBitmap(userIcon);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(fingerPrintRect.width, fingerPrintRect.height);
                params.leftMargin = fingerPrintRect.x ;
                params.topMargin = fingerPrintRect.y;
                if(iv.getParent() == null) {
                    rl.addView(iv, params);
                    iv.setImageBitmap(fingerPrintIcon);
                }//else
                //((RelativeLayout)rl.getParent()).removeView(iv);
            }
        });
        ////////////////////////****************************************************** END

        if(imgCinVerso.area() > 300000 && imgCinVerso.area() <600000) {
            //draw the adress
            int i = ((imgCinVerso.height/4) - (imgCinVerso.height/5));
            adressCinV = new Rect(imgCinVerso.x,(imgCinVerso.height/4)+i+imgCinVerso.y,imgCinVerso.width/2 + imgCinVerso.width/5,imgCinVerso.height/5);
            rectangle(mRgba, adressCinV.tl(), adressCinV.br(), CONTOUR_WHITE, 1);
          /*  //draw the barcode zone
            Rect barcodeZone = new Rect(imgContour.width / 4 + imgContour.x, (4 * (imgContour.height / 5)) + imgContour.y, imgContour.width / 2, imgContour.height / 6);
            rectangle(rgb, barcodeZone.tl(), barcodeZone.br(), new Scalar(255, 0, 0), 2);
            //draw the number left in verso CIN
            Rect numberLeft = new Rect(imgContour.width / 12 + imgContour.x, (6 * (imgContour.height / 7)) + imgContour.y, imgContour.width / 17, imgContour.height / 12);
            rectangle(rgb, numberLeft.tl(), numberLeft.br(), new Scalar(255, 0, 0), 2);
            //Imgproc.circle(rgb,numberLeft.tl(),25, new Scalar(255, 0, 0), 2);
            //draw the number right in verso CIN
            Rect numberRight = new Rect((imgContour.width - imgContour.width / 7) + imgContour.x, (6 * (imgContour.height / 7)) + imgContour.y, imgContour.width / 17, imgContour.height / 12);
            rectangle(rgb, numberRight.tl(), numberRight.br(), new Scalar(255, 0, 0), 2);
            //Imgproc.circle(rgb,numberRight.tl(),25, new Scalar(255, 0, 0), 2);*/
        }
    }


    public void detectMRZJava(Mat rgb) {
        Mat gray = new Mat();
        cvtColor(rgb, gray, COLOR_BGR2GRAY);

        Mat grad = new Mat();
        Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, new Size(3, 3));
        morphologyEx(gray, grad, MORPH_GRADIENT, morphKernel);

        Mat bw = new Mat();
        threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);

        // connect horizontally oriented regions
        Mat connected = new Mat();
        morphKernel = getStructuringElement(MORPH_RECT, new Size(9, 1));
        morphologyEx(bw, connected, MORPH_CLOSE, morphKernel);

        // find contours
        Mat mask = zeros(bw.size(), CV_8UC1);
        List<MatOfPoint> contours = new LinkedList<>();
        findContours(connected, contours, new Mat(), RETR_CCOMP, CHAIN_APPROX_SIMPLE, new Point(0, 0));

        List<Mat> mrz = new LinkedList<>();
        double r = 0;
        // filter contours
        for (int i = 0; i < contours.size(); i++) {
            Rect rect = boundingRect(contours.get(i));
            r = rect.height != 0 ? (double)(rect.width/rect.height) : 0;
            if ((rect.width > connected.cols() * .5) && /* filter from rect width */
                    (r > 15) && /* filter from width:hight ratio */
                    (r < 30) /* filter from width:hight ratio */
                    ) {

                Mat a = new Mat();
                mrz.get(i).push_back(a.col(rect.x));
                rectangle(rgb, rect.tl(),rect.br(), new Scalar(0, 255, 0), 1);
            } else {
                //rectangle(rgb, rect, Scalar(0, 0, 255), 1);
            }
        }
      /*  if (2 == mrz.size() || 3 == mrz.size()) {
            Mat mrz0 = mrz.get(0);
            Mat mrz1 = 3 == mrz.size() ? mrz.get(2) : mrz.get(1);
            // just assume we have found the two data strips in MRZ and combine them
            Rect max = CvMaxRect(mrz0, mrz1);
            rectangle(rgb, max, Scalar(255, 0, 0), 2);  // draw the MRZ
        }*/
    }

    public void detectCardContourJava(Mat mRgba){

        Mat gray = new Mat();
        cvtColor( mRgba, gray, COLOR_RGBA2GRAY ); //Convert to gray
        Mat thr = new Mat();
        threshold( gray, thr, 125, 255, THRESH_BINARY); //Threshold the gray

        List<MatOfPoint> contours = new LinkedList<>(); // Vector for storing contours

        findContours( thr, contours,new Mat(), RETR_CCOMP, CHAIN_APPROX_SIMPLE ); // Find the contours in the image

        Rect imgContour = biggestCountour(contours); // Find the bounding rectangle for biggest contour
        //drawContours( rgb, contours,largest_contour_index, Scalar( 0, 255, 0 ), 2 ); // Draw the largest contour using previously stored index.
        rectangle(mRgba, imgContour.br(), imgContour.tl(), CONTOUR_WHITE, 1);

        if(detectMode == StartActivity.DETECTCINR)
            detectRedFlag(mRgba, imgContour);
        else
            if(detectMode == StartActivity.DETECTCINV){
                detectBlack(mRgba, imgContour);
            }


       /* Log.e("card contours","imgContour "+imgContour+ " | imgContour.size().height "+imgContour.size().height
        +" | imgContour.size().width "+imgContour.size().width+ " | imgContour.height "+imgContour.height
        + " | imgContour.width "+imgContour.width+ " | tl().X "+imgContour.tl().x+ " | tl().Y "+imgContour.tl().y
        + " | br().x "+imgContour.br().x +" | tl().y"+imgContour.tl().y);
*/
    }

    public void detectBlack(Mat mRgba, Rect cardContour){
        Mat imgHSV = new Mat();
        cvtColor(mRgba, imgHSV, COLOR_RGB2HSV);
        Core.inRange(imgHSV, new Scalar(0, 0, 0, 0), new Scalar(180, 255, 30, 0), imgHSV);
        Mat mask = imgHSV;
        List<MatOfPoint> contoursBlack = new ArrayList<>();
        Imgproc.findContours(mask, contoursBlack, new Mat(), Imgproc.RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
        Rect blackContour = biggestCountour(contoursBlack);

        Rect fingerCin = new Rect((cardContour.width/2)+(cardContour.width/6)+cardContour.x,(cardContour.height/4)+cardContour.y ,cardContour.width/3, cardContour.height/3);
        //rectangle(mRgba, fingerCin.tl(), fingerCin.br(), new Scalar(255, 0, 0), 1);
        //rectangle(mRgba, blackContour.tl(), blackContour.br(), new Scalar(255, 0, 255), 1);

        if(fingerCin.contains(blackContour.tl())) {
            //rectangle(mRgba, blackContour.tl(), blackContour.br(), new Scalar(255, 0, 255), 1);
            drawCinAddressContoursV(mRgba, cardContour);

            addCinBitmap = setBitmapCIN(adressCinV, addCinBitmap, mRgba);
        }
    }

    public void detectRedFlag (Mat mRgba, Rect cardContour){
        Mat gray = new Mat();
        cvtColor(mRgba, gray, COLOR_RGB2HSV);

        //detect red flag
        Mat red = new Mat();
        Core.inRange(gray, new Scalar(170, 70, 50), new Scalar(180, 255, 255), red);

        Mat mask = red;
        List<MatOfPoint> contoursRed = new ArrayList<>();
        Imgproc.findContours(mask, contoursRed, new Mat(), Imgproc.RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

        for (MatOfPoint contour: contoursRed) {
            Imgproc.fillPoly(mask, Arrays.asList(contour), new Scalar(255, 255, 255));
        }

        Rect flagContour = biggestCountour(contoursRed);
        if(flagContour.area() > 11000 &&  flagContour.area() < 15000 && cardContour.contains(flagContour.tl())) {
            //rectangle(mRgba, flagContour.br(), flagContour.tl(), new Scalar(0, 0, 255), 2);
            drawCinTextContours(mRgba,cardContour);

            numCinBitmap = setBitmapCIN(numCin, numCinBitmap, mRgba);
            nomCinBitmap = setBitmapCIN(nomCin, nomCinBitmap, mRgba);
            prenomCinBitmap = setBitmapCIN(prenom1Cin, prenomCinBitmap, mRgba);
            prenom2CinBitmap = setBitmapCIN(prenom2Cin, prenom2CinBitmap, mRgba);
            birthdayCinBitmap = setBitmapCIN(bithdayCin, birthdayCinBitmap, mRgba);
            cityCinBitmap = setBitmapCIN(cityCin, cityCinBitmap, mRgba);
        }
/*
        //detect black text
        Mat black = new Mat();
        Core.inRange(gray, new Scalar(0, 0, 0), new Scalar(179,50, 100), black);
        List<MatOfPoint> contoursBlack = new ArrayList<>();
        Mat maskBlack = black;

        Imgproc.findContours(maskBlack, contoursBlack, new Mat(), Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_TC89_KCOS);
        for (MatOfPoint contour: contoursBlack)
            Imgproc.fillPoly(maskBlack, Arrays.asList(contour), new Scalar( 255, 255, 255, 255));
        //Imgproc.drawContours(mRgba,contoursBlack, -1, new Scalar(255, 0, 0), 1);

        Rect imgContour = biggestCountour(contoursBlack);
        rectangle(mRgba, imgContour.br(), imgContour.tl(), new Scalar(0, 0, 255), 2);



        for(int i = 0; i < contoursBlack.size(); i++) {

           /* Rect rect = Imgproc.boundingRect(contoursBlack.get(i));
            Mat maskROI = new Mat(maskBlack, rect);
            maskROI.setTo(new Scalar(0, 0, 0));
            Imgproc.drawContours(maskBlack, contoursBlack, i, new Scalar(255, 255, 255), Core.FILLED);
            // ratio of non-zero pixels in the filled region
            double r = (double)countNonZero(maskROI)/(rect.width*rect.height);

            if (r > .45
                    &&
                    (rect.height > 5 && rect.width > 20)
                    )
            {
                rectangle(mRgba, rect.br(), rect.tl(), new Scalar(0, 255, 0), 2);
            }*/
          /* Rect bounding = new Rect();
            bounding = Imgproc.boundingRect(contoursBlack.get(i));
            rectangle(mRgba, bounding.br(), bounding.tl(), new Scalar(0, 255, 255), 1);
        }*/

    }

    public void drawCinTextContours(Mat mRgba, Rect cardContour){

        int y = cardContour.height/4 - cardContour.height/5;
        //(x,y,w,h)
        /* numCin = new Rect(cardContour.width/3+cardContour.x, cardContour.height/3+cardContour.y,
                 cardContour.width/3,cardContour.height/10);
*/
         nomCin = new Rect(cardContour.width/3+cardContour.x,(cardContour.height/2-y)+cardContour.y,
                 cardContour.width/2+cardContour.width/17,cardContour.height/9);

         prenom1Cin = new Rect(cardContour.width/3+cardContour.x, cardContour.height/2+y+cardContour.y,
                 cardContour.width-350 ,cardContour.height/10);

         prenom2Cin = new Rect(cardContour.width/3+cardContour.x, (cardContour.height/2+(3*y))+cardContour.y,
                 cardContour.width-cardContour.width/3, cardContour.height/10);

         bithdayCin = new Rect(cardContour.width/3+cardContour.x, (3*(cardContour.height/4))+cardContour.y,
                 cardContour.width/2,cardContour.height/10);

         cityCin = new Rect(cardContour.width/3+cardContour.x,(5*(cardContour.height/6))+cardContour.y,
                 cardContour.width/2+cardContour.width/17 ,cardContour.height/10);

        //rectangle(mRgba, numCin.tl(), numCin.br(), CONTOUR_WHITE, 1);
        rectangle(mRgba, nomCin.tl(), nomCin.br(), CONTOUR_WHITE, 1);
        rectangle(mRgba, prenom1Cin.tl(), prenom1Cin.br(), CONTOUR_WHITE, 1);
        rectangle(mRgba, prenom2Cin.tl(), prenom2Cin.br(),CONTOUR_WHITE, 1);
        rectangle(mRgba, bithdayCin.tl(), bithdayCin.br(), CONTOUR_WHITE, 1);
        rectangle(mRgba, cityCin.tl(), cityCin.br(),CONTOUR_WHITE, 1);

       /* msgDetect = "[CIN RECTO DETECTED !]";
        putText(mRgba,msgDetect,new Point(cardContour.tl().x - 20, cardContour.tl().y -20), FONT_HERSHEY_SIMPLEX, 1.0, new Scalar(0, 200, 0),2);
*/
    }
    public void drawCinAddressContoursV(Mat mRgba, Rect cardContour){

        int x = ((cardContour.height/4) - (cardContour.height/5));

        adressCinV = new Rect(cardContour.x,(cardContour.height/4)+x+cardContour.y,cardContour.width/2 + cardContour.width/5,cardContour.height/5);
        rectangle(mRgba, adressCinV.tl(), adressCinV.br(), CONTOUR_WHITE, 1);

        msgDetect = "[CIN VERSO DETECTED !]";
        putText(mRgba, msgDetect, new Point(cardContour.tl().x - 20, cardContour.tl().y - 20), FONT_HERSHEY_SIMPLEX, 1.0, new Scalar(0, 200, 0), 2);

    }
    public Bitmap setBitmapCIN (Rect cin, Bitmap cinBitmap, Mat mRgba){

        Mat numCinMat = new Mat();
        if(cin.width != 0 && cin.height != 0)
            numCinMat = mRgba.submat(cin);
        Mat binarized = new Mat();
   /*     Rect template = new Rect(120,128,701,452);

        MatOfPoint2f src = new MatOfPoint2f(
                cin.tl(), // tl
                new Point(cin.width,cin.y), // tr
                cin.br(), // br
                new Point(cin.x,cin.height) // bl
        );

        MatOfPoint2f dst = new MatOfPoint2f(
                new Point(0,0), // awt has a Point class too, so needs canonical name here
                new Point(100,0),
                new Point(100,100),
                new Point(0,100)
        );

        Mat m = getPerspectiveTransform(src,dst);
        warpPerspective(mRgba,numCinMat.clone(),m,dst.size());
*/
        Mat gray = new Mat();
        Mat erode = new Mat();
        cvtColor(numCinMat, gray, COLOR_RGBA2GRAY);
        //threshold(gray, binarized, 125, 255, THRESH_BINARY | THRESH_OTSU);
        adaptiveThreshold(gray,binarized,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,15,25);
       // Mat kernel = getStructuringElement(MORPH_CROSS, new Size(3, 3));
        //erode(binarized, erode, kernel);
       // numCinMat.convertTo(binarized, -1, 2, 50);
        if(numCinMat.height() != 0 && numCinMat.width() != 0) {
            cinBitmap = Bitmap.createBitmap(binarized.width(), binarized.height(), Bitmap.Config.ARGB_8888);
            if(cinBitmap != null)
                Utils.matToBitmap(binarized,cinBitmap);
        }
        return cinBitmap;
    }

    public void detectCardContour(Mat mRgba){
        Mat gray = new Mat() ;

        Mat small;
        cvtColor(mRgba, gray, COLOR_RGBA2GRAY);

        Mat morph = new Mat();
        Mat kernel = getStructuringElement(MORPH_ELLIPSE, new Size(11, 11));
        morphologyEx(gray, morph, MORPH_OPEN, kernel);

        Mat bw = new Mat();
        threshold(morph, bw, 0, 255.0, THRESH_BINARY | THRESH_OTSU);

        Mat bdry = new Mat();
        kernel = getStructuringElement(MORPH_ELLIPSE, new Size(3, 3));
        erode(bw, bdry, kernel);
        subtract(bw, bdry, bdry);
        List<MatOfPoint> contours = new ArrayList<>();
        findContours(bdry, contours,new Mat(),Imgproc.RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
        Rect imgContour = biggestCountour(contours);
        rectangle(mRgba, imgContour.br(), imgContour.tl(), new Scalar(0, 255, 0), 2);
    }

    public MatOfPoint biggestCountousMatOfPoint(List<MatOfPoint> contours){

        double maxValCoun = 0;
        MatOfPoint largestContours = new MatOfPoint();
        for (int i = 0; i < contours.size(); i++)
        {
            double cArea = contourArea(contours.get(i));
            if (maxValCoun < cArea)
            {
                maxValCoun = cArea;
                largestContours = contours.get(i);
            }
        }
        return largestContours;
    }

    public static Rect biggestCountour(List<MatOfPoint> contours){

        Rect bounding = new Rect();
        double maxValCoun = 0;
        for (int i = 0; i < contours.size(); i++)
        {
            double cArea = contourArea(contours.get(i));
            if (maxValCoun < cArea)
            {
                maxValCoun = cArea;
                bounding = boundingRect(contours.get(i));
            }
        }
        return bounding;
    }

    public static void drawRotatedRect(Mat image, RotatedRect rotatedRect, Scalar color, int thickness) {
        Point[] vertices = new Point[4];
        rotatedRect.points(vertices);
        MatOfPoint points = new MatOfPoint(vertices);
       // drawContours(image, Arrays.asList(points), -1, color, thickness);
        for (int j = 0; j < 4; j++){
            Imgproc.line(image, vertices[j], vertices[(j+1)%4], new Scalar(0,255,0));
        }
    }

    private  void detectText(Mat mRgba){

        Scalar CONTOUR_COLOR = new Scalar(255);
        MatOfKeyPoint keypoint = new MatOfKeyPoint();
        List<KeyPoint> listpoint = new ArrayList<KeyPoint>();
        KeyPoint kpoint = new KeyPoint();
        Mat mask = zeros(mGray.size(), CV_8UC1);
        int rectanx1;
        int rectany1;
        int rectanx2;
        int rectany2;

        //
        Scalar zeos = new Scalar(0, 0, 0);
        List<MatOfPoint> contour1 = new ArrayList<MatOfPoint>();
        List<MatOfPoint> contour2 = new ArrayList<MatOfPoint>();
        Mat kernel = new Mat(1, 50, CV_8UC1, Scalar.all(255));
        Mat morbyte = new Mat();
        Mat hierarchy = new Mat();

        Rect rectan2 = new Rect();//
        Rect rectan3 = new Rect();//
        int imgsize = mRgba.height() * mRgba.width();
        //
        FeatureDetector detector = FeatureDetector
                .create(FeatureDetector.MSER);
        detector.detect(mGray, keypoint);
        listpoint = keypoint.toList();
        //
        for (int ind = 0; ind < listpoint.size(); ind++) {
            kpoint = listpoint.get(ind);
            rectanx1 = (int) (kpoint.pt.x - 0.5 * kpoint.size);
            rectany1 = (int) (kpoint.pt.y - 0.5 * kpoint.size);
            // rectanx2 = (int) (kpoint.pt.x + 0.5 * kpoint.size);
            // rectany2 = (int) (kpoint.pt.y + 0.5 * kpoint.size);
            rectanx2 = (int) (kpoint.size);
            rectany2 = (int) (kpoint.size);
            if (rectanx1 <= 0)
                rectanx1 = 1;
            if (rectany1 <= 0)
                rectany1 = 1;
            if ((rectanx1 + rectanx2) > mGray.width())
                rectanx2 = mGray.width() - rectanx1;
            if ((rectany1 + rectany2) > mGray.height())
                rectany2 = mGray.height() - rectany1;
            Rect rectant = new Rect(rectanx1, rectany1, rectanx2, rectany2);
            Mat roi = new Mat(mask, rectant);
            roi.setTo(CONTOUR_COLOR);

        }

        Imgproc.morphologyEx(mask, morbyte, Imgproc.MORPH_DILATE, kernel);
        Imgproc.findContours(morbyte, contour2, hierarchy,
                Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
        for (int ind = 0; ind < contour2.size(); ind++) {
            rectan3 = boundingRect(contour2.get(ind));
            if (rectan3.area() > 0.5 * imgsize || rectan3.area() < 100
                    || rectan3.width / rectan3.height < 2) {
                Mat roi = new Mat(morbyte, rectan3);
                roi.setTo(zeos);

            } else
                rectangle(mRgba, rectan3.br(), rectan3.tl(),
                        CONTOUR_COLOR);
        }

    }

    private void SaveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public native void detectMRZNative(long matAddrRgba, String path);
    public native void detectMRZthersode(long matAddrRgba, long matAddrGray);
    public native void detectCIN (long matAddrRgba);
    public native void detectTextNative (long matAddrRgba);
    public native void detectBlackTextNative(long matAddrRgba);
    public native void detectCardContourNative(long matAddrRgba);

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

       /* Rect textCinRecto = new Rect(imgCinRecto.width / 3 + imgCinRecto.x,imgCinRecto.height/3 + imgCinRecto.y ,
                                        imgCinRecto.width/3,imgCinRecto.height/3);*/

       /* Rect textCinRecto = new Rect(imgCinRecto.x + (imgCinRecto.width - (imgCinRecto.width/3)), imgCinRecto.y + (imgCinRecto.height - (imgCinRecto.height/3)),
                imgCinRecto.width - (imgCinRecto.width/3),imgCinRecto.height - (imgCinRecto.height/3));*/
     /*   Rect rectRecto = rotatedRectRecto.boundingRect();

        if (detectMode == StartActivity.DETECTCINR) {
            cinBitmapR = setBitmapCIN(textCinRecto, cinBitmapR, mRgba);

            if (cinBitmapR != null) {
                //cinBitmapR != null ? cinBitmapR : cinBitmapV);
                ByteArrayOutputStream stream11 = new ByteArrayOutputStream();
                Intent resultIntent = new Intent(DetectActivity.this, ResultActivity.class);


                cinBitmapR.compress(Bitmap.CompressFormat.PNG, 100, stream11);
                byte[] cinRArray = stream11.toByteArray();
                resultIntent.putExtra("cinRecto", cinRArray);
                resultIntent.putExtra("DETECTMODE", detectMode);
                startActivity(resultIntent);
                finish();
            }
        }else{
        if (detectMode == StartActivity.DETECTCINV) {

            cinBitmapV = setBitmapCIN(imgCinVerso, cinBitmapV, mRgba);

             if ( cinBitmapV != null) {
                ByteArrayOutputStream stream12 = new ByteArrayOutputStream();
                Intent resultIntent1 = new Intent(DetectActivity.this, ResultActivity.class);

                    cinBitmapV.compress(Bitmap.CompressFormat.PNG, 100, stream12);
                    byte[] cinRArray = stream12.toByteArray();
                    resultIntent1.putExtra("cinVerso", cinRArray);
                    resultIntent1.putExtra("DETECTMODE", detectMode);
                    startActivity(resultIntent1);
                    finish();
                }
            }
            }*/
/*
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
        ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
        ByteArrayOutputStream stream3 = new ByteArrayOutputStream();
        ByteArrayOutputStream stream4 = new ByteArrayOutputStream();
        ByteArrayOutputStream stream5 = new ByteArrayOutputStream();
        ByteArrayOutputStream stream6 = new ByteArrayOutputStream();

            Intent resultActivity = new Intent(DetectActivity.this, ResultActivity.class);

            if((numCinBitmap != null && nomCinBitmap != null && prenomCinBitmap != null &&
                    prenom2CinBitmap != null && birthdayCinBitmap != null && cityCinBitmap != null) || addCinBitmap != null) {

                if(detectMode == StartActivity.DETECTCINR) {
                    numCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    nomCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream1);
                    prenomCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                    prenom2CinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream3);
                    birthdayCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream4);
                    cityCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream5);

                    byte[] numCinArray = stream.toByteArray();
                    byte[] nomCinArray = stream1.toByteArray();
                    byte[] prenomCinArray = stream2.toByteArray();
                    byte[] prenom2CinArray = stream3.toByteArray();
                    byte[] birthdayCinArray = stream4.toByteArray();
                    byte[] cityCinArray = stream5.toByteArray();

                    resultActivity.putExtra("numCin", numCinArray);
                    resultActivity.putExtra("nomCin", nomCinArray);
                    resultActivity.putExtra("prenomCin", prenomCinArray);
                    resultActivity.putExtra("prenom2Cin", prenom2CinArray);
                    resultActivity.putExtra("birthdayCin", birthdayCinArray);
                    resultActivity.putExtra("cityCin", cityCinArray);
                }
                else if(detectMode == StartActivity.DETECTCINV){
                    addCinBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream6);
                    resultActivity.putExtra("addrCin", stream6.toByteArray());
                }
                    resultActivity.putExtra("DETECTMODE", detectMode);

                startActivity(resultActivity);
                finish();
            }*/
        return true;
    }


}
